package com.walterfinkbeiner.apitestscucumber.miniframework;

import okhttp3.ResponseBody;
import retrofit2.Converter;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * @author Walter Finkbeiner
 */

public class TestUtils {

    public static <U> U convertResponse(Type type, ResponseBody responseBody) throws IOException {
        Converter<ResponseBody, U> converter =
                RetrofitSingleton.getInstance().responseBodyConverter(type, new Annotation[0]);
        return converter.convert(responseBody);
    }
}
