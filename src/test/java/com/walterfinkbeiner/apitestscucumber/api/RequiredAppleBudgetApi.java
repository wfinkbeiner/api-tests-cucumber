package com.walterfinkbeiner.apitestscucumber.api;


import com.walterfinkbeiner.apitestscucumber.model.RequiredAppleBudget;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author Walter Finkbeiner
 */
public interface RequiredAppleBudgetApi {
    @GET("/requiredAppleBudget")
    Call<RequiredAppleBudget> getRequiredAppleBudget(@Query("date") String date, @Query("days") String days);
}
