package com.walterfinkbeiner.apitestscucumber.model;

/**
 * @author Walter Finkbeiner
 */
public class CustomError {

    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
