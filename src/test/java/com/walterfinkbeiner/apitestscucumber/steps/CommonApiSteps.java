package com.walterfinkbeiner.apitestscucumber.steps;

import com.walterfinkbeiner.apitestscucumber.model.CustomError;
import com.walterfinkbeiner.apitestscucumber.steps.state.SharedState;
import com.walterfinkbeiner.apitestscucumber.miniframework.TestUtils;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * @author Walter Finkbeiner
 */
public class CommonApiSteps {

    private SharedState sharedState;

    public CommonApiSteps(SharedState sharedState) {
        this.sharedState = sharedState;
    }

    @Then("^the status code of the response should be (.*)$")
    public void theStatusCodeOfTheResponseShouldBe(int expectedCode) {
        int actualCode = sharedState.getResponse().code();
        assertEquals(String.format("The status code of the response should be %s", expectedCode), expectedCode, actualCode);
    }

    @And("^the error message should be \"(.*)\"$")
    public void theErrorMessageShouldBe(String expectedError) throws IOException {
        CustomError customError = TestUtils.convertResponse(CustomError.class, sharedState.getResponse().errorBody());
        String actualError = customError.getError();
        assertEquals(String.format("The error body of the response should be '%s'", expectedError), expectedError, actualError);
    }
}
