package com.walterfinkbeiner.apitestscucumber.steps;

import com.walterfinkbeiner.apitestscucumber.api.RequiredAppleBudgetApi;
import com.walterfinkbeiner.apitestscucumber.model.RequiredAppleBudget;
import com.walterfinkbeiner.apitestscucumber.steps.state.SharedState;
import com.walterfinkbeiner.apitestscucumber.miniframework.StepsForApi;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import retrofit2.Response;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * @author Walter Finkbeiner
 */
public class RequiredAppleBudgetApiSteps extends StepsForApi<RequiredAppleBudgetApi> {

    private SharedState sharedState;

    private String daysQueryParameter;
    private String dateQueryParameter;

    public RequiredAppleBudgetApiSteps(SharedState sharedState) {
        this.sharedState = sharedState;
    }

    @Given("^the 'date' query parameter for the request is (.*)$")
    public void theDateQueryParameterForTheRequestIs(String dateQueryParameter) {
        this.dateQueryParameter = dateQueryParameter;
    }

    @Given("^the 'days' query parameter for the request is (.*)$")
    public void theDaysQueryParameterForTheRequestIs(String daysQueryParameter) {
        this.daysQueryParameter = daysQueryParameter;
    }

    @When("^I make a GET request to /requiredAppleBudget$")
    public void iMakeAGETRequestToRequiredAppleBudget() throws IOException {
        sharedState.setResponse(getApi().getRequiredAppleBudget(dateQueryParameter, daysQueryParameter).execute());
    }

    @SuppressWarnings("unchecked")
    @And("^the budget should be (.+)$")
    public void theBudgetShouldBe(double expectedBudget) {
        Double actualBudget = ((Response<RequiredAppleBudget>) sharedState.getResponse()).body().getBudget();
        assertEquals(String.format("The budget should be %s", expectedBudget), expectedBudget, actualBudget, 0);
    }
}
