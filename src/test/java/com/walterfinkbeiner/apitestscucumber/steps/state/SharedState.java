package com.walterfinkbeiner.apitestscucumber.steps.state;

import retrofit2.Response;

/**
 * @author Walter Finkbeiner
 */
public class SharedState {

    private Response<?> response;

    public Response<?> getResponse() {
        return response;
    }

    public void setResponse(Response<?> response) {
        this.response = response;
    }

}
