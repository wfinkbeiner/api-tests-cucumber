package com.walterfinkbeiner.apitestscucumber.test;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * @author Walter Finkbeiner
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features",
        glue = "com.walterfinkbeiner.apitestscucumber.steps",
        plugin = {"pretty", "html:target/cucumber-reports/html", "junit:target/cucumber-reports/junit/Cucumber.xml"})
public class CucumberTest {
}
