Feature: Calculate how much budget is required to purchase an Apple for a set number of days.

  Days 1-7 of a given month, the apple costs 0.05 per day.
  On Days 8-14 the apple costs 0.07 per day
  On days 15-21 the apple is at 0.08 per day.
  All other days the apple is at 0.04 per day.

  API accepts 2 parameters, date and days.
  Date is the start date in format of yyyymmdd.
  Days is the number of days as an integer to budget for.
  The max number of days the API accepts is 365.
  The API only accepts valid dates in the calendar.
  Any incorrect date or days and the API returns a 400 error code.

  Scenario Outline: Making a valid request to the API
    Given the 'date' query parameter for the request is <date>
    And the 'days' query parameter for the request is <days>
    When I make a GET request to /requiredAppleBudget
    Then the status code of the response should be <code>
    And the budget should be <budget>

    Examples:
      | date     | days | code | budget |
      | 20180801 | 1    | 200  | 0.05   |
      | 20180101 | 365  | 200  | 21.32  |
      | 20200228 | 2    | 200  | 0.08   |
      | 20200229 | 1    | 200  | 0.04   |
      | 20180820 | 20   | 200  | 0.98   |
      | 20180801 | 10   | 200  | 0.56   |
      | 20180101 | 7    | 200  | 0.35   |
      | 20180808 | 7    | 200  | 0.49   |
      | 20180815 | 7    | 200  | 0.56   |
      | 20180822 | 10   | 200  | 0.40   |


  Scenario Outline: Making an invalid request to the API with all query parameters
    Given the 'date' query parameter for the request is <date>
    And the 'days' query parameter for the request is <days>
    When I make a GET request to /requiredAppleBudget
    Then the status code of the response should be <code>
    And the error message should be "<error>"

    Examples:
      | date     | days   | code | error                                      |
      | 20180140 | 1      | 400  | '20180140' is not a valid value for 'date' |
      | 20180229 | 1      | 400  | '20180229' is not a valid value for 'date' |
      | 2018012  | 1      | 400  | '2018012' is not a valid value for 'date'  |
      | 20180810 | string | 400  | 'string' is not a valid value for 'days'   |
      | 20180810 | -1     | 400  | 'days' must be greater than 0              |
      | 20180810 | 0      | 400  | 'days' must be greater than 0              |
      | 20180810 | 366    | 400  | 'days' must be equal or lower than 365     |


  Scenario: Making an invalid request to the API without the 'days' query parameter
    Given the 'date' query parameter for the request is 20180808
    When I make a GET request to /requiredAppleBudget
    Then the status code of the response should be 400
    And the error message should be "Required parameter 'days' is not present"

  Scenario: Making an invalid request to the API without the 'date' query parameter
    Given the 'days' query parameter for the request is 1
    When I make a GET request to /requiredAppleBudget
    Then the status code of the response should be 400
    And the error message should be "Required parameter 'date' is not present"
